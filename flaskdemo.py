from flask import Flask
from flask import Flask, jsonify, render_template, request, Response
from flask_cors import CORS, cross_origin

app = Flask(__name__)
CORS(app)




@app.route('/python_end_point_with_header' ,methods=['GET','POST'])
@cross_origin()
def material_maker_calc():

    packing_fraction       = request.args.get('packing_fraction'       , 0, type=float)

    new_packing_fraction=packing_fraction*2

    print 'packing_fraction=',new_packing_fraction

    return jsonify(result=new_packing_fraction) 
    #return '13'





@app.route('/python_end_point_with_url_arg/<name>')
@cross_origin()
def hello(name):

    
    return jsonify(result=name*2)





@app.route('/index_with_plotly')
def index_with_plotly():
    return render_template('index_with_plotly.html')


@app.route('/index')
def index():
    return render_template('index.html')